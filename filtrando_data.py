from data_ch import DATA

def run():
    all_python_devs = [worker["name"] for worker in DATA
    if worker["language"] == "python"]

    all_Platzi_workers = [worker["name"] for worker in DATA 
    if worker["organization"] == "Platzi"]

    adults = list(filter(lambda worker: worker["age"] > 18, DATA))
    adults = list(map(lambda worker: worker["name"], adults))
    # old_people = list(map(lambda worker: worker | {"old": worker["age"] > 70}, DATA)) #Only works on python 3.9
    old_people = list(map(lambda worker: {**worker, **{"old": worker["age"] > 70}}, DATA))


    for worker in old_people:
        print(worker)



if __name__ == '__main__':
    run()

#Reto: crear las listas all_python_devs y all_platzi workers usando una combinación de filter y map
#Crear lista de old_people y adults con list_comprehensions