#Reto: crear las listas all_python_devs y all_platzi workers usando una combinación de filter y map
#Crear lista de old_people y adults con list_comprehensions

from data_ch import DATA

def function():
    all_python_devs = list(filter(lambda worker: worker["language"] == "python" , DATA))
    all_python_devs = list(map(lambda worker: worker["name"],all_python_devs))

    all_Platzi_workers = list(filter (lambda worker: worker["organization"] == "Platzi", DATA))
    all_Platzi_workers = list(map (lambda worker: worker["name"],all_Platzi_workers))

    adults = [worker["name"] for worker in DATA
    if worker["age"] > 18]

    # old_people = [{**worker, **{"old": worker["age"] > 70}} for worker in DATA] python3.8
    old_people = [worker | {"old": worker["age"] > 70} for worker in DATA] #python3.9    
    for worker in all_python_devs:
        print(worker)

if __name__ == '__main__':
    function()