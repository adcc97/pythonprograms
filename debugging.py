def divisors(num):
    try:
        if num < 0:
           print("ingresa solo numeros positivos")
            # raise ValueError('Ingresa solo numeros positivos')
        divisors = [i for i in range(1,num+1) if num % i == 0]
        
        return divisors
    except ValueError:
        print("Termina el programa")
    
   
   

def run():


    try:
        num = int(input("Ingresa un numero: "))
        
        print(divisors(num))
        print("Termino el programa")
    except ValueError:
        print("Debes ingresar un numero")

if __name__ == '__main__':
    run()