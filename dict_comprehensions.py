import math

def run():
    # numeros_nat={i**3 for i in range(1,101)}
    # num_nat={}
    # for i in range(1,101):
    #     if i%3 !=0:
    #         num_nat[i] = i**3
    num_nat={i: i**3 for i in range(1,101) if i%3 !=0}
        

    print(num_nat)
if __name__ == '__main__':
    run()

#Escribir un dictionary comprehension cuyas llaves sean los primeros 1000 numeros naturales con sus 
#raices cuadradas como valores