#Escribir un dictionary comprehension cuyas llaves sean los primeros 1000 numeros naturales con sus 
#raices cuadradas como valores
import math

def sqr():
    num_nat ={i: math.sqrt(i) for i in range(1,1000)}
    print(num_nat)

if __name__ == '__main__':
    sqr()