def divisors(num):
    
           # raise ValueError('Ingresa solo numeros positivos')
      

        divisors = [i for i in range(1,num+1) if num % i == 0]
        
        return divisors
     
    
    
   
   

def run():
    contador = 0
    while True:

        try:
            num = int(input("Ingresa un numero: "))
            if num <=0:
                raise ValueError
            print(divisors(num))
            print("Termino el programa")
            break
        except ValueError:
            print("Ingresa solo numeros positivos")

if __name__ == '__main__':
    run()